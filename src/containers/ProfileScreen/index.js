
import React, { Component } from 'react'
import { StyleSheet, View, Text, Image,ImageBackground } from 'react-native'

export default class ProfileScreen extends Component {

  render() {

    return (

      <View style={styles.container}>
        <View style={{ height: 250 }}>
          <ImageBackground source={require('../../images/user-profile-bg.jpg')} style={styles.backgroundStyle}> 
          <Image source={require('../../images/img_avatar.png')} style={{
            borderRadius: 85,
            borderColor:'#00FFFF',
            borderWidth: 2,
            height: 170,
            width: 170,
          }} />
          <Text style={styles.TextStyle}>John Doe</Text>
          </ImageBackground>
    
        </View>
        <Text>

        </Text>
      </View>
    )
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundStyle:{
    flex:1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'

  },
  TextStyle:{
   fontSize:17,
   color:'#fff',
   marginTop:12
  }
})
