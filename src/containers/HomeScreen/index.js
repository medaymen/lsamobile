import React, { Component } from 'react'
import { StyleSheet, Text, ScrollView ,Image} from 'react-native'
import PropTypes from 'prop-types';
import { createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation'
import ProfileScreen from '../ProfileScreen/index'
import FirstScreen from '../FirstScreen/index'
import { View } from 'react-native-animatable';

/**
 * Just a centered logout button.
 */
const CustomDrawerNavigator = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={{height:200,backgroundColor:'#555',alignItems:'center',justifyContent:'center'}}>
      <Image source={require('../../images/img_avatar.png')} style={{height:120,width:120,borderRadius:60}}/>
       <Text style={styles.Text}>John Doe</Text>
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)
const AppDrawerNav = createDrawerNavigator({
  Profile: ProfileScreen,
  Home:FirstScreen


},
  {
    contentComponent: CustomDrawerNavigator
  }
)
export default class HomeScreen extends Component {
  static propTypes = {
    logout: PropTypes.func
  }

  render() {
    return (
     
       <AppDrawerNav />


    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    backgroundColor: '#1976D2',
    margin: 20
  },
  Text:{
    color : 'white',
    paddingTop: 5
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold'
  }
})
