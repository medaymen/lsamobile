import { AsyncStorage } from "react-native"
export const AsyncStrg ={
    _storeData : async (token) => {
        try {
          await AsyncStorage.setItem('access-token', token);
        } catch (error) {
          // Error saving data
        }
      },
      retrieveData : async (key) => {
        try {
          const value = await AsyncStorage.getItem(key);
          if (value !== null) {
            // We have data!!
            console.log(value);
          }
         } catch (error) {
           // Error retrieving data
         }
      }

}